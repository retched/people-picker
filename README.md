# Come On Down Picker

This project is essentially a random number generator, with a twist. If you run any kind of raffle contest or selection, this program will allow you to select such a person at random. There is no "fixing" or chance of leaving off a ticket or three, every person put into the pool is selectable. The program will even keep track of those selected in this session.

## Getting Started

The program will run on a Windows PC (Windows 2000, Windows XP, Windows 7, Windows 8, Windows 10) with any Visual Basic compiler. (Microsoft Visual Studio is the preferred compiler. But if your compiler can handle `.sln` files, you should be fine.) Additionally, the program uses Windows Media Player to play sound files so you must have the interop assemblies relating to that.

### Installing

To install the program simply download the Zip File and extract the .EXE to a directory of your choosing. No need for fonts as they are automatically embedded. You will need to have the .NET Framework 4.6.2 installed.

## Built With

* [Microsoft Visual Studio 2015 Community](https://www.visualstudio.com//) - The IDE used.
* *Visual Basic* - The language of the project.


## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/retched/people-picker/tags). 
* 1.0.0 - Initial release.

## Authors

* **Paul Williams** - *Initial work* - [retched](https://gitlab.com/retched)

See also the list of [contributors](https://gitlab.com/retched/people-picker/project_members) who participated in this project.

## License

This project is licensed under the Eclipse Public License - see the [LICENSE](LICENSE) file for details

## Coming Soon

* More game show skins.
* The ability to resize the game presenter screen to full-screen.
* Possible change to a graphics engine like [Unity](https://unity3d.com/) or [Unreal Engine](https://www.unrealengine.com/what-is-unreal-engine-4).

## Acknowledgments

* thorklein - For sitting through all my Twitch streams while making this project.
* My friend Scott G. - For giving me the idea (that I already had in my mind) to make this.
* The [Who Wants To Be A Millionaire Fan Forums](http://regisfan.proboards.com) (specifically Paul G.) for helping me with the font information about the show itself.
