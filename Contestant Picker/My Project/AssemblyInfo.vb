﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("People Picker")>
<Assembly: AssemblyDescription("A way to randomly choose someone to come on down.")>
<Assembly: AssemblyCompany("IWL Entertainment")>
<Assembly: AssemblyProduct("RandomSelector")>
<Assembly: AssemblyCopyright("Copyright ©  2016, Paul Williams")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("fa83ec2a-b4dd-4160-8d77-ee35912ddd22")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.1.0")>
<Assembly: AssemblyFileVersion("1.0.1.0")>
