﻿
Public Class frmPresenter
    Friend intBlinker As Integer = 0

    Private Sub pbNameFrame_Resize(sender As Object, e As EventArgs) Handles pbNameFrame.Resize
        lblContestantName.Location = New Point(pnlContestantName.Width * 0.1225, 7)
    End Sub

    Private Sub miHideBorder_Click(sender As Object, e As EventArgs) Handles miHideBorder.Click
        miHideBorder.Checked = Not miHideBorder.Checked

        If miHideBorder.Checked = True Then
            ' Set up for full screen...

            Me.FormBorderStyle = FormBorderStyle.None
            'Me.WindowState = FormWindowState.Maximized
            'Me.Bounds = Screen.FromControl(Me).Bounds
            'Me.SizeGripStyle = SizeGripStyle.Auto
        Else
            Me.FormBorderStyle = FormBorderStyle.FixedToolWindow
            'Me.WindowState = FormWindowState.Maximized
            'Me.SizeGripStyle = SizeGripStyle.Hide
        End If

    End Sub

    Private Sub frmPresenter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblCallToAction.Font = ConduitITCBold.GetInstance(24, FontStyle.Regular)
        lblContestantName.Font = ConduitITCBold.GetInstance(35, FontStyle.Regular)
        lblCounter.Font = ConduitITCBold.GetInstance(15, FontStyle.Regular)
        lblWelcome.Font = ConduitITCBold.GetInstance(35, FontStyle.Regular)
    End Sub

    Private Sub tmrContestantBlink_Tick(sender As Object, e As EventArgs) Handles tmrContestantBlink.Tick

        intBlinker += 1

        ' cause the text to blink or hide
        If intBlinker Mod 2 Then
            lblContestantName.Visible = False
        Else
            lblContestantName.Visible = True
        End If

        If intBlinker = 30 Then
            intBlinker = 0
            tmrContestantBlink.Enabled = False
        End If

    End Sub

    Private Sub ContextMenuStrip1_Opening(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles ContextMenuStrip1.Opening
        miHideBorder.Checked = IIf(MyBase.FormBorderStyle = FormBorderStyle.None, True, False)
    End Sub

End Class