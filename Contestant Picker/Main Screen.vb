﻿Imports System.IO
Imports WMPLib

Public Class frmMainScreen
    Dim intContestantCount As Integer = 0
    Dim intSelectedContestant As Integer
    Dim WithEvents wmpTrack1 As WindowsMediaPlayer = New WindowsMediaPlayer
    Dim WithEvents wmpTrack2 As WindowsMediaPlayer = New WindowsMediaPlayer
    Dim WithEvents wmpLive1 As WindowsMediaPlayer = New WindowsMediaPlayer
    Dim WithEvents wmpLive2 As WindowsMediaPlayer = New WindowsMediaPlayer
    Dim strTrack1 As String = ""
    Dim strTrack2 As String = ""
    Dim strLive As String = ""
    Dim WithEvents frmDisplay As New frmPresenter
    Dim blnDisplayed As Boolean = False
    Dim intLineCount As Integer = 0

    Private Sub btnAddContestant_Click(sender As Object, e As EventArgs) Handles btnAddContestant.Click

        Dim strPlayerName As String = txtContestantName.Text.Trim

        If Not String.IsNullOrWhiteSpace(strPlayerName) Then
            ' Four steps.

            ' Step 0: Increase Player Count
            intContestantCount += 1

            ' Step 1: Add contents of txtPlayerName to dgvPlayerList
            If CheckContestantList(strPlayerName) = False Then
                dgvContestantPool.Rows.Add({strPlayerName, intContestantCount})
            Else
                MessageBox.Show("Duplicate Entry Found. Not added to list.", "ERROR - Duplicate Entry", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            ' Step 2: Clear txtbox
            txtContestantName.Text = Nothing
        End If

        ' Focus txtbox and put cursor to end
        txtContestantName.Focus()
        txtContestantName.Select(txtContestantName.Text.Length, 0)

    End Sub

    Private Sub txtContestantName_KeyDown(sender As Object, e As KeyEventArgs) Handles txtContestantName.KeyDown
        If e.KeyCode = Keys.Enter Then
            btnAddContestant.PerformClick()
            e.Handled = True
            e.SuppressKeyPress = True
        End If
    End Sub

    Private Sub txtLowest_TextChanged(sender As Object, e As EventArgs) Handles txtLowest.TextChanged, txtHighest.TextChanged

        Dim charactersAllowed As String = "0123456789"

        Dim theText As String = sender.Text
        Dim LetterA As String
        Dim SelectionIndex As Integer = sender.SelectionStart
        Dim Change As Integer

        For x As Integer = 0 To sender.TextLength - 1
            LetterA = sender.Text.Substring(x, 1)
            If charactersAllowed.Contains(LetterA) = False Then
                theText = theText.Replace(LetterA, String.Empty)
                Change = 1
            End If
        Next

        sender.Text = theText

        If SelectionIndex - Change < 0 Then
            sender.Select(0, 0)
        Else
            sender.Select(SelectionIndex - Change, 0)
        End If

        btnAddBatch.Enabled = (Not String.IsNullOrEmpty(txtLowest.Text) And Not String.IsNullOrEmpty(txtHighest.Text))

    End Sub

    Private Sub btnAddBatch_Click(sender As Object, e As EventArgs) Handles btnAddBatch.Click
        Dim intStartNumber As Integer
        Dim intEndNumber As Integer

        If CInt(txtHighest.Text) < CInt(txtLowest.Text) Then
                intStartNumber = txtHighest.Text
                intEndNumber = txtLowest.Text
            Else
                intStartNumber = txtLowest.Text
                intEndNumber = txtHighest.Text
            End If

            For i As Integer = intStartNumber To intEndNumber
                Dim strPlayerName As String

                If rbContestant.Checked Then
                    strPlayerName = "Contestant #" & i.ToString
                Else
                    strPlayerName = "Ticket #" & i.ToString
                End If

                ' Four steps.
                ' Step 0: Increase Player Count
                intContestantCount += 1

                ' Step 1: Add contents of txtPlayerName to dgvPlayerList
                If CheckContestantList(strPlayerName) = False Then
                    dgvContestantPool.Rows.Add({strPlayerName, intContestantCount})
                End If

                ' Step 2: Clear txtbox
                txtContestantName.Text = Nothing
            Next

            MessageBox.Show((intEndNumber - intStartNumber + 1).ToString & " Person(s) was added to the contestant pool.")

    End Sub

    Private Function CheckContestantList(ByVal strPlayerName As String) As Boolean

        Dim blnExists As Boolean = False

        ' Loop through the entire list in the DataGridView and see if this exists.

        For Each item In dgvContestantPool.Rows

            If strPlayerName = item.Cells("ContestantName").Value Then
                blnExists = True
                Exit For
            End If

        Next

        Return blnExists
    End Function

    Private Sub txtLogoFileName_TextChanged(sender As Object, e As EventArgs) Handles txtLogoFileName.TextChanged
        If File.Exists(txtLogoFileName.Text) Then
            If isValidImage(txtLogoFileName.Text) Then
                ' If the file is either a JPG, PNG or GIF image, then load that image into picturebox
                pbPictureBox.Image = Image.FromFile(txtLogoFileName.Text)

                ' If this is a valid image, set the picture box of the presenter to that image.
                frmDisplay.pbLogo.Image = Image.FromFile(txtLogoFileName.Text)
            Else
                pbPictureBox.Image = Nothing
                frmDisplay.pbLogo.Image = Nothing
            End If
        Else
            pbPictureBox.Image = Nothing
            frmDisplay.pbLogo.Image = Nothing
        End If
    End Sub
    Public Function isValidImage(xFile As String) As Boolean
        If String.IsNullOrEmpty(xFile) Then
            Return False
        Else
            Try
                Dim img As Image = Image.FromFile(xFile)

                ' If the image is either a JPEG, PNG, or GIF file... Return True
                Return img.RawFormat.Equals(Imaging.ImageFormat.Jpeg) Or img.RawFormat.Equals(Imaging.ImageFormat.Png) Or img.RawFormat.Equals(Imaging.ImageFormat.Gif)

            Catch generatedExceptionName As OutOfMemoryException
                Return False
            End Try
        End If

    End Function

    Private Sub btnBrowseLogo_Click(sender As Object, e As EventArgs) Handles btnBrowseLogo.Click
        If ofdLogoSearch.ShowDialog() = DialogResult.OK Then
            txtLogoFileName.Text = ofdLogoSearch.FileName()
        End If
    End Sub

    Private Sub dgvContestantPool_RowsAdded(sender As Object, e As DataGridViewRowsAddedEventArgs) Handles dgvContestantPool.RowsAdded

        ' Step 3: Update Player Count
        lblPlayerCount.Text = dgvContestantPool.Rows.Count.ToString
        lblPoolCount.Text = dgvContestantPool.Rows.Count.ToString

        frmDisplay.lblCounter.Text = "Total Contestants: " & intContestantCount.ToString

        If dgvContestantPool.Rows.Count > 1 Then
            btnStartRandomizer.Enabled = True
        Else
            btnStartRandomizer.Enabled = False
        End If
    End Sub

    Private Sub btnDeleteContestant_Click(sender As Object, e As EventArgs) Handles btnDeleteContestant.Click
        ' Confirm with the user if you want to delete these rows... otherwise ignore.

        If MessageBox.Show("You are about to delete " & dgvContestantPool.SelectedRows.Count & " contestant(s) from the contestant pool. Be sure this is what you want to do!", "Deletion Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) = DialogResult.Yes Then
            For Each item In dgvContestantPool.SelectedRows
                dgvContestantPool.Rows.RemoveAt(item.Index)
            Next
        End If

    End Sub

    Private Sub btnClearPool_Click(sender As Object, e As EventArgs) Handles btnClearPool.Click
        If MessageBox.Show("You are about to clear the entire contestant pool, is this what you want to do?", "Deletion Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.Yes Then
            dgvContestantPool.Rows.Clear()
            dgvContestantPool.ClearSelection()

            intContestantCount = 0
        End If
    End Sub

    Private Sub dgvContestantPool_RowsRemoved(sender As Object, e As DataGridViewRowsRemovedEventArgs) Handles dgvContestantPool.RowsRemoved

        ' Step 3: Update Player Count
        lblPlayerCount.Text = dgvContestantPool.Rows.Count.ToString
        lblPoolCount.Text = dgvContestantPool.Rows.Count.ToString

        If dgvContestantPool.Rows.Count > 1 Then
            btnStartRandomizer.Enabled = True
        Else
            btnStartRandomizer.Enabled = False
        End If

    End Sub

    Private Sub dgvContestantPool_SelectionChanged(sender As Object, e As EventArgs) Handles dgvContestantPool.SelectionChanged
        btnDeleteContestant.Enabled = (dgvContestantPool.SelectedRows.Count > 0)
    End Sub

    Private Sub btnShowPresenter_Click(sender As Object, e As EventArgs) Handles btnShowPresenter.Click

        ' Start playing the background music.
        btnPlayBackground.PerformClick()

        ' In each of these, there is:
        ' ... a background
        ' ... a box to display "The Next Contestant Is..."
        ' ... a box to display the selected contestant.
        ' Fan-fare tracks...

        ' TODO: Select the theme of the presenter...

        ' Set up the labels of the new presenter.
        frmDisplay.lblCallToAction.Text = txtCallToAction.Text.Trim
        frmDisplay.lblContestantName.Text = lblSelectedPlayer.Text.Trim
        frmDisplay.lblCounter.Text = "Total Contestants: " & intContestantCount.ToString
        frmDisplay.lblWelcome.Text = txtWelcomeText.Text.Trim

        frmDisplay.pbLogo.Image = pbPictureBox.Image

        ' Show the new form
        frmDisplay.Show()

    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Environment.Exit(0)
    End Sub

    Private Sub dgvContestantPool_CellValidating(sender As Object, e As DataGridViewCellValidatingEventArgs) Handles dgvContestantPool.CellValidating

        If CheckContestantList(dgvContestantPool.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue) = True And dgvContestantPool.IsCurrentCellDirty = True Then
            'Your code goes here
            MessageBox.Show("Entered value already exists in the list, try again or delete the entry.", "INVALID DATA - Already Exists", MessageBoxButtons.OK, MessageBoxIcon.Error)


            e.Cancel = True
        End If
    End Sub

    Private Sub btnClearWinners_Click(sender As Object, e As EventArgs) Handles btnClearWinners.Click
        ' Reset the Winners' DGV
        dgvSelected.Rows.Clear()

    End Sub

    Private Sub btnStartRandomizer_Click(sender As Object, e As EventArgs) Handles btnStartRandomizer.Click

        dgvContestantPool.ReadOnly = True
        pnlPoolControls.Enabled = False

        wmpTrack1.controls.stop()
        wmpTrack2.controls.stop()
        'wmpLive1.controls.stop()
        wmpLive2.controls.stop()

        wmpTrack1 = New WindowsMediaPlayer
        wmpTrack2 = New WindowsMediaPlayer

        btnStopRandomizer.Enabled = True
        btnStartRandomizer.Enabled = False

        tmrClock.Enabled() = True
        'btnResetWinner.Visible = True

        frmDisplay.tmrContestantBlink.Enabled = False
        frmDisplay.lblContestantName.Visible = True
        frmDisplay.lblContestantName.ForeColor = Color.White

        ' Disable the Add buttons
        tbPool.Enabled = False

        ' Start Playing the Selection Music
        btnPlayDrums.PerformClick()

        frmDisplay.lblContestantName.ForeColor = Color.White

    End Sub

    Private Sub btnStopRandomizer_Click(sender As Object, e As EventArgs) Handles btnStopRandomizer.Click
        dgvContestantPool.ReadOnly = False
        pnlPoolControls.Enabled = True

        btnResetWinner.Visible = True

        btnStopRandomizer.Enabled = False
        btnStartRandomizer.Enabled = True

        tmrClock.Enabled() = False

        ' Enable the Add buttons
        tbPool.Enabled = True

        ' Now that the Randomizer has STOPPED, this means a winner was chosen...
        ' Add that winner onto the list.
        dgvSelected.Rows.Add({dgvContestantPool.Rows(intSelectedContestant).Cells(0).Value, dgvContestantPool.Rows(intSelectedContestant).Cells(1).Value})

        ' If the checkbox is checked, remove that player from consideration.
        If cbClearSelected.Checked Then
            dgvContestantPool.Rows.RemoveAt(intSelectedContestant)
        End If

        ' Pause the track
        wmpLive1.controls.stop()

        ' Play the "Selection"
        btnPlaySelection.PerformClick()

        ' Change the text to Gold
        frmDisplay.lblContestantName.ForeColor = Color.Gold

        ' Blink the winner!
        frmDisplay.tmrContestantBlink.Enabled = True

        For Each item In dgvSelected.Rows
            item.Selected = False
        Next
    End Sub

    Private Sub tmrClock_Tick(sender As Object, e As EventArgs) Handles tmrClock.Tick

        Dim intIndex As Integer = GenRandomInt(0, dgvContestantPool.RowCount - 1)
        Dim strContestantName As String = dgvContestantPool.Rows(intIndex).Cells("ContestantName").Value.ToString()

        ' Set the value of the intSelectedContestant to random number
        intSelectedContestant = intIndex

        ' Set the label in the controller equal to the player's name
        lblSelectedPlayer.Text = strContestantName

        '' If the Presenter is being show... then we need to update the text of that presenter on each of these ticks.
        'frmDisplay.lblContestantName.Text = strContestantName

    End Sub

    ''' <summary>
    ''' Generates a random Integer with any (inclusive) minimum or (inclusive) maximum values, with full range of Int32 values.
    ''' </summary>
    ''' <param name="inMin">Inclusive Minimum value. Lowest possible return value.</param>
    ''' <param name="inMax">Inclusive Maximum value. Highest possible return value.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GenRandomInt(inMin As Int32, inMax As Int32) As Int32
        Static staticRandomGenerator As New System.Random
        If inMin > inMax Then Dim t = inMin : inMin = inMax : inMax = t
        If inMax < Int32.MaxValue Then Return staticRandomGenerator.Next(inMin, inMax + 1)
        ' now max = Int32.MaxValue, so we need to work around Microsoft's quirk of an exclusive max parameter.
        If inMin > Int32.MinValue Then Return staticRandomGenerator.Next(inMin - 1, inMax) + 1 ' okay, this was the easy one.
        ' now min and max give full range of integer, but Random.Next() does not give us an option for the full range of integer.
        ' so we need to use Random.NextBytes() to give us 4 random bytes, then convert that to our random int.
        Dim bytes(3) As Byte ' 4 bytes, 0 to 3
        staticRandomGenerator.NextBytes(bytes) ' 4 random bytes
        Return BitConverter.ToInt32(bytes, 0) ' return bytes converted to a random Int32
    End Function

    Private Sub btnPlayBackground_Click(sender As Object, e As EventArgs) Handles btnPlayBackground.Click

        ' Stop playback
        wmpLive1.controls.stop()
        wmpLive1 = New WindowsMediaPlayer

        ' Set the URL...
        wmpLive1.URL = txtBackgroundMusic.Text.Trim

        ' Set to repeat
        wmpLive1.settings.setMode("loop", True)

        ' Start playback
        wmpLive1.controls.play()

        strLive = "BG"
    End Sub

    Private Sub btnPlayDrums_Click(sender As Object, e As EventArgs) Handles btnPlayDrums.Click

        ' Stop playback
        wmpLive2.controls.stop()
        wmpLive2 = New WindowsMediaPlayer

        ' Set the URL...
        wmpLive2.URL = txtDrumRoll.Text.Trim

        ' Set to repeat
        wmpLive2.settings.setMode("loop", True)

        ' Start playback
        wmpLive2.controls.play()

        'strLive = "DR"

    End Sub

    Private Sub btnPlaySelection_Click(sender As Object, e As EventArgs) Handles btnPlaySelection.Click
        ' Stop playback
        wmpLive2.controls.stop()
        wmpLive2 = New WindowsMediaPlayer

        ' Set the URL...
        wmpLive2.URL = txtSelectionRoll.Text.Trim

        ' Set to repeat
        wmpLive2.settings.setMode("loop", False)

        ' Start playback
        wmpLive2.controls.play()

        strLive = "SE"
    End Sub

    Private Sub btnKillSounds_Click(sender As Object, e As EventArgs) Handles btnKillSounds.Click
        strLive = Nothing

        wmpTrack1.controls.stop()
        wmpTrack2.controls.stop()
        wmpLive1.controls.stop()
        wmpLive2.controls.stop()


    End Sub

    Private Sub btnPreviewDrumRoll_Click(sender As Object, e As EventArgs) Handles btnPreviewSelectionRoll.Click, btnPreviewDrumRoll.Click, btnPreviewBackgroundMusic.Click


        Dim clickedButton As Button = CType(sender, Button)

        ' On Click          = Track 1
        ' On Shift Click    = Keep on Repeat (Track 1)
        ' On Alt Click      = Track 2

        ' If the track is playing on Track 1 or Track 2, IGNORE attempts at playing it on the other track.
        If strTrack1 = clickedButton.Tag AndAlso wmpTrack1.playState = WMPPlayState.wmppsPlaying Then
            ' This means we're Playing the GameIntro on Track 1.... And we need to stop it

            ' Stop playing the track.
            wmpTrack1.controls.stop()
            wmpTrack1 = New WindowsMediaPlayer

            strTrack1 = ""

            ' Reset the image to the play button.
            clickedButton.Image = My.Resources.Resources.small_play_button
        ElseIf strTrack2 = clickedButton.Tag AndAlso wmpTrack2.playState = WMPPlayState.wmppsPlaying Then
            ' This means we're Playing the GameIntro on Track 2.... And we need to stop it

            ' Stop playing the track.
            wmpTrack2.controls.stop()
            wmpTrack2 = New WindowsMediaPlayer

            strTrack2 = ""

            ' Reset the image to the play button.
            clickedButton.Image = My.Resources.Resources.small_play_button
        Else
            ' So we're NOT playing this...  We NEED to be playing this...


            If Control.ModifierKeys = Keys.Alt Then

                If My.Computer.FileSystem.FileExists(GetTextBoxValue(clickedButton.Tag.ToString)) Then

                    ' Find the old preview button that is marked as STOP, and change that to a PLAY button instead since we're stopping the track.
                    Dim btns = Controls.Find("btnPreview" & strTrack2, True)
                    If btns.Count > 0 Then
                        Dim btn As Button = btns.First
                        btn.Image = My.Resources.small_play_button
                    End If

                    ' Stop playing what's already being played.
                    wmpTrack2.controls.stop()
                    wmpTrack2 = New WindowsMediaPlayer

                    ' Someone is holding the ALT key, this means that we're trying to put this on the Track2.

                    ' Set this Tag to GameIntro
                    strTrack2 = clickedButton.Tag.ToString

                    ' Set this Track's URL to the file.
                    wmpTrack2.URL = GetTextBoxValue(strTrack2)

                    ' Start playing the track.
                    wmpTrack2.controls.play()

                    ' Set the button to have the STOP icon.
                    clickedButton.Image = My.Resources.Resources.black_stop_button
                End If
            ElseIf Control.ModifierKeys = Keys.Shift Then
                If My.Computer.FileSystem.FileExists(GetTextBoxValue(clickedButton.Tag.ToString)) Then

                    ' Find the old preview button that is marked as STOP, and change that to a PLAY button instead since we're stopping the track.
                    Dim btns = Controls.Find("btnPreview" & strTrack1, True)
                    If btns.Count > 0 Then
                        Dim btn As Button = btns.First
                        btn.Image = My.Resources.small_play_button
                    End If

                    ' A Shift click means REPEAT on Track 1...
                    ' Start playing the old track.
                    wmpTrack1.controls.stop()
                    wmpTrack1 = New WindowsMediaPlayer

                    ' Set this Tag to GameIntro
                    strTrack1 = clickedButton.Tag.ToString

                    ' PUT ON REPEAT.
                    wmpTrack1.settings.setMode("loop", True)

                    ' Set this Track's URL to the file.
                    wmpTrack1.URL = GetTextBoxValue(strTrack1)

                    ' Start playing the track.
                    wmpTrack1.controls.play()

                    ' Set the button to have the STOP icon.
                    clickedButton.Image = My.Resources.Resources.black_stop_button
                End If

            Else

                If My.Computer.FileSystem.FileExists(GetTextBoxValue(clickedButton.Tag.ToString)) Then

                    ' Find the old preview button that is marked as STOP, and change that to a PLAY button instead since we're stopping the track.
                    Dim btns = Controls.Find("btnPreview" & strTrack1, True)
                    If btns.Count > 0 Then
                        Dim btn As Button = btns.First
                        btn.Image = My.Resources.small_play_button
                    End If

                    ' Set this Tag to GameIntro
                    strTrack1 = clickedButton.Tag.ToString

                    ' Start playing the old track.
                    wmpTrack1.controls.stop()
                    wmpTrack1 = New WindowsMediaPlayer

                    ' Set this Track's URL to the file.
                    wmpTrack1.URL = GetTextBoxValue(strTrack1)

                    ' DO NOT PUT ON REPEAT.
                    wmpTrack1.settings.setMode("loop", False)

                    ' Start playing the track.
                    wmpTrack1.controls.play()

                    ' Set the button to have the STOP icon.
                    clickedButton.Image = My.Resources.Resources.black_stop_button

                End If

            End If
        End If


    End Sub
    Private Function GetTextBoxValue(ByVal fieldTag As String) As String

        Dim output As String = ""

        ' Find the TextBox with the aforementioned "tag"
        Dim txtb = Controls.Find("txt" & fieldTag, True)
        If txtb.Count > 0 Then
            Dim txt As TextBox = txtb.First
            output = txt.Text.Trim
        End If

        Return output

    End Function

    Private Sub btnBrowseDrumRoll_Click(sender As Object, e As EventArgs) Handles btnBrowseSelectionRoll.Click, btnBrowseDrumRoll.Click, btnBrowseBackgroundMusic.Click


        Dim clickedButton As Button = CType(sender, Button)

        If MusicFileDialog.ShowDialog() = DialogResult.OK Then
            SetTextBoxValue(clickedButton.Tag.ToString, MusicFileDialog.FileName)
        End If
    End Sub
    Private Sub SetTextBoxValue(ByVal fieldtag As String, ByVal input As String)

        ' Find the TextBox with the aforementioned "tag"
        Dim txtb = Controls.Find("txt" & fieldtag, True)
        If txtb.Count > 0 Then
            Dim txt As TextBox = txtb.First
            txt.Text = input.Trim
        End If

    End Sub
    Private Sub wmpTrack1_PlayStateChange(ByVal NewState As Integer) Handles wmpTrack1.PlayStateChange
        If NewState = WMPPlayState.wmppsStopped OrElse NewState = WMPPlayState.wmppsMediaEnded Then
            ' If the music STOPPED or ENDED then... change the button icon back to a PLAY icon.
            Dim btns = Controls.Find("btnPreview" & strTrack1, True)
            If btns.Count > 0 Then
                Dim btn As Button = btns.First
                btn.Image = My.Resources.small_play_button
            End If

        ElseIf NewState = WMPPlayState.wmppsPlaying Then
            Dim btns = Controls.Find("btnPreview" & strTrack1, True)
            If btns.Count > 0 Then
                Dim btn As Button = btns.First
                btn.Image = My.Resources.black_stop_button
            End If
        End If
    End Sub

    Private Sub wmpTrack2_PlayStateChange(ByVal NewState As Integer) Handles wmpTrack2.PlayStateChange
        If NewState = WMPPlayState.wmppsStopped OrElse NewState = WMPPlayState.wmppsMediaEnded Then
            ' If the music STOPPED or ENDED then... change the button icon back to a PLAY icon.
            Dim btns = Controls.Find("btnPreview" & strTrack2, True)
            If btns.Count > 0 Then
                Dim btn As Button = btns.First
                btn.Image = My.Resources.small_play_button
            End If

        ElseIf NewState = WMPPlayState.wmppsPlaying Then
            Dim btns = Controls.Find("btnPreview" & strTrack2, True)
            If btns.Count > 0 Then
                Dim btn As Button = btns.First
                btn.Image = My.Resources.black_stop_button
            End If

        End If
    End Sub
    Private Sub wmpLive_PlayStateChange(ByVal NewState As Integer) Handles wmpLive2.PlayStateChange
        If NewState = WMPPlayState.wmppsStopped OrElse NewState = WMPPlayState.wmppsMediaEnded Then
            ' If the music track STOPPED and this is playing the select music, play the Background Music

            If strLive = "SE" Then
                'wmpLive1.controls.play()
                btnPlayBackground.PerformClick()

            End If

        End If
    End Sub
    Private Sub frmDisplay_Closed() Handles frmDisplay.Closed
        blnDisplayed = False
        'MessageBox.Show("Hey it closed!")

        ' Hey the form closed! That's okay. Change the details that we need BACK.
        frmDisplay = New frmPresenter

    End Sub

    Private Sub btnBrowseTextFile_Click(sender As Object, e As EventArgs) Handles btnBrowseTextFile.Click
        If ofdPlainText.ShowDialog() = DialogResult.OK Then
            txtTextFileName.Text = ofdPlainText.FileName
        End If
    End Sub

    Private Sub txtTextFileName_TextChanged(sender As Object, e As EventArgs) Handles txtTextFileName.TextChanged
        ' The text has been change... Let's go through and check it now IF IT exists

        If File.Exists(txtTextFileName.Text) Then
            intLineCount = File.ReadAllLines(txtTextFileName.Text).Count(Function(x) Not String.IsNullOrWhiteSpace(x))
        Else
            intLineCount = 0
        End If

        If (intLineCount > 0) Then
            btnAddFromFile.Enabled = True

            lblFileCount.Text = intLineCount.ToString & " Players"
        Else
            btnAddFromFile.Enabled = False

            lblFileCount.Text = "Empty File"
        End If


    End Sub

    Private Sub btnAddFromFile_Click(sender As Object, e As EventArgs) Handles btnAddFromFile.Click
        Dim sr As StreamReader = New StreamReader(txtTextFileName.Text.Trim)

        Dim intPlayerAdded As Integer = 0

        Do While sr.Peek() >= 0

            Dim strPlayerName As String = sr.ReadLine()

            If Not String.IsNullOrWhiteSpace(strPlayerName) Then
                ' Four steps.

                ' Step 1: Add contents of the string to dgvPlayerList
                If CheckContestantList(strPlayerName) = False Then

                    ' Step 0: Increase Player Count
                    intContestantCount += 1
                    intPlayerAdded += 1

                    dgvContestantPool.Rows.Add({strPlayerName, intContestantCount})
                End If
            End If
        Loop

        MessageBox.Show("Finished!" & vbNewLine & vbNewLine & intLineCount.ToString & " Lines Read." & vbNewLine & intPlayerAdded & " Players Added To Pool." & vbNewLine & vbNewLine & "If these numbers are not the same, check your file for duplicate or invalid entries.", "Batch Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)

        ' Step 2: Clear txtbox of the filename
        txtTextFileName.Text = Nothing

    End Sub

    Private Sub btnClearWinner_Click(sender As Object, e As EventArgs) Handles btnResetWinner.Click
        lblSelectedPlayer.Text = "Waiting to choose..."
        frmDisplay.lblContestantName.ForeColor = Color.White
        frmDisplay.lblContestantName.Visible = True
        frmDisplay.tmrContestantBlink.Enabled = False

        btnResetWinner.Visible = False
    End Sub

    Private Sub lblSelectedPlayer_TextChanged(sender As Object, e As EventArgs) Handles lblSelectedPlayer.TextChanged
        frmDisplay.lblContestantName.Text = lblSelectedPlayer.Text
    End Sub

    Private Sub cbFrameDisplay_CheckedChanged(sender As Object, e As EventArgs) Handles cbFrameDisplay.CheckedChanged
        If cbFrameDisplay.Checked Then
            frmDisplay.FormBorderStyle = FormBorderStyle.FixedToolWindow
        Else
            frmDisplay.FormBorderStyle = FormBorderStyle.None
        End If
    End Sub

    Private Sub lblPlayerCount_Click(sender As Object, e As EventArgs) Handles lblPlayerCount.TextChanged
        intContestantCount = dgvContestantPool.Rows.Count
        frmDisplay.lblCounter.Text = "Total Contestants: " & intContestantCount.ToString
    End Sub
    Private Sub frmPresenter_lblWelcome_Paint(sender As Object, e As PaintEventArgs) Handles frmDisplay.Paint
        If frmDisplay.FormBorderStyle = FormBorderStyle.FixedToolWindow Then
            cbFrameDisplay.Checked = True
        Else
            cbFrameDisplay.Checked = False
        End If
    End Sub
End Class
