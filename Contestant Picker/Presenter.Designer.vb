﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPresenter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.miHideBorder = New System.Windows.Forms.ToolStripMenuItem()
        Me.pnlContestantName = New System.Windows.Forms.Panel()
        Me.pnlNameFrame = New System.Windows.Forms.Panel()
        Me.lblContestantName = New System.Windows.Forms.Label()
        Me.pbNameFrame = New System.Windows.Forms.PictureBox()
        Me.pnlActionFrame = New System.Windows.Forms.Panel()
        Me.lblCallToAction = New System.Windows.Forms.Label()
        Me.tmrContestantBlink = New System.Windows.Forms.Timer(Me.components)
        Me.pnlTopRow = New System.Windows.Forms.Panel()
        Me.pnlRightTop = New System.Windows.Forms.Panel()
        Me.pbLogo = New System.Windows.Forms.PictureBox()
        Me.pnlLeftTop = New System.Windows.Forms.Panel()
        Me.pnlTopLeftTop = New System.Windows.Forms.Panel()
        Me.lblWelcome = New System.Windows.Forms.Label()
        Me.pnlTopLeftBottom = New System.Windows.Forms.Panel()
        Me.lblCounter = New System.Windows.Forms.Label()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.pnlContestantName.SuspendLayout()
        Me.pnlNameFrame.SuspendLayout()
        CType(Me.pbNameFrame, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlActionFrame.SuspendLayout()
        Me.pnlTopRow.SuspendLayout()
        Me.pnlRightTop.SuspendLayout()
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlLeftTop.SuspendLayout()
        Me.pnlTopLeftTop.SuspendLayout()
        Me.pnlTopLeftBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miHideBorder})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(174, 26)
        '
        'miHideBorder
        '
        Me.miHideBorder.Name = "miHideBorder"
        Me.miHideBorder.Size = New System.Drawing.Size(173, 22)
        Me.miHideBorder.Text = "&Hide Border Frame"
        '
        'pnlContestantName
        '
        Me.pnlContestantName.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlContestantName.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnlContestantName.BackColor = System.Drawing.Color.Transparent
        Me.pnlContestantName.Controls.Add(Me.pnlNameFrame)
        Me.pnlContestantName.Controls.Add(Me.pbNameFrame)
        Me.pnlContestantName.Location = New System.Drawing.Point(0, 250)
        Me.pnlContestantName.Name = "pnlContestantName"
        Me.pnlContestantName.Size = New System.Drawing.Size(1008, 158)
        Me.pnlContestantName.TabIndex = 1
        '
        'pnlNameFrame
        '
        Me.pnlNameFrame.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlNameFrame.BackColor = System.Drawing.Color.FromArgb(CType(CType(43, Byte), Integer), CType(CType(42, Byte), Integer), CType(CType(41, Byte), Integer))
        Me.pnlNameFrame.Controls.Add(Me.lblContestantName)
        Me.pnlNameFrame.Location = New System.Drawing.Point(122, 59)
        Me.pnlNameFrame.Name = "pnlNameFrame"
        Me.pnlNameFrame.Size = New System.Drawing.Size(768, 90)
        Me.pnlNameFrame.TabIndex = 4
        '
        'lblContestantName
        '
        Me.lblContestantName.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblContestantName.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContestantName.ForeColor = System.Drawing.Color.White
        Me.lblContestantName.Location = New System.Drawing.Point(0, 0)
        Me.lblContestantName.Name = "lblContestantName"
        Me.lblContestantName.Size = New System.Drawing.Size(768, 90)
        Me.lblContestantName.TabIndex = 5
        Me.lblContestantName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblContestantName.UseCompatibleTextRendering = True
        '
        'pbNameFrame
        '
        Me.pbNameFrame.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbNameFrame.Image = Global.RandomSelector.My.Resources.Resources.Next_Contestant
        Me.pbNameFrame.Location = New System.Drawing.Point(0, 0)
        Me.pbNameFrame.Name = "pbNameFrame"
        Me.pbNameFrame.Size = New System.Drawing.Size(1008, 158)
        Me.pbNameFrame.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pbNameFrame.TabIndex = 3
        Me.pbNameFrame.TabStop = False
        '
        'pnlActionFrame
        '
        Me.pnlActionFrame.BackColor = System.Drawing.Color.Transparent
        Me.pnlActionFrame.Controls.Add(Me.lblCallToAction)
        Me.pnlActionFrame.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlActionFrame.Location = New System.Drawing.Point(0, 414)
        Me.pnlActionFrame.Name = "pnlActionFrame"
        Me.pnlActionFrame.Size = New System.Drawing.Size(1008, 123)
        Me.pnlActionFrame.TabIndex = 3
        '
        'lblCallToAction
        '
        Me.lblCallToAction.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblCallToAction.Font = New System.Drawing.Font("Microsoft Sans Serif", 32.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCallToAction.ForeColor = System.Drawing.Color.White
        Me.lblCallToAction.Location = New System.Drawing.Point(0, 0)
        Me.lblCallToAction.Name = "lblCallToAction"
        Me.lblCallToAction.Size = New System.Drawing.Size(1008, 123)
        Me.lblCallToAction.TabIndex = 0
        Me.lblCallToAction.Text = "Be sure to enter for your chance to play!"
        Me.lblCallToAction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblCallToAction.UseCompatibleTextRendering = True
        '
        'tmrContestantBlink
        '
        Me.tmrContestantBlink.Interval = 500
        '
        'pnlTopRow
        '
        Me.pnlTopRow.BackColor = System.Drawing.Color.Transparent
        Me.pnlTopRow.Controls.Add(Me.pnlRightTop)
        Me.pnlTopRow.Controls.Add(Me.pnlLeftTop)
        Me.pnlTopRow.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlTopRow.Location = New System.Drawing.Point(0, 0)
        Me.pnlTopRow.Name = "pnlTopRow"
        Me.pnlTopRow.Size = New System.Drawing.Size(1008, 244)
        Me.pnlTopRow.TabIndex = 4
        '
        'pnlRightTop
        '
        Me.pnlRightTop.Controls.Add(Me.pbLogo)
        Me.pnlRightTop.Dock = System.Windows.Forms.DockStyle.Right
        Me.pnlRightTop.Location = New System.Drawing.Point(507, 0)
        Me.pnlRightTop.Name = "pnlRightTop"
        Me.pnlRightTop.Size = New System.Drawing.Size(501, 244)
        Me.pnlRightTop.TabIndex = 1
        '
        'pbLogo
        '
        Me.pbLogo.Location = New System.Drawing.Point(20, 20)
        Me.pbLogo.Margin = New System.Windows.Forms.Padding(10)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(460, 205)
        Me.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbLogo.TabIndex = 0
        Me.pbLogo.TabStop = False
        '
        'pnlLeftTop
        '
        Me.pnlLeftTop.Controls.Add(Me.pnlTopLeftTop)
        Me.pnlLeftTop.Controls.Add(Me.pnlTopLeftBottom)
        Me.pnlLeftTop.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlLeftTop.Location = New System.Drawing.Point(0, 0)
        Me.pnlLeftTop.Name = "pnlLeftTop"
        Me.pnlLeftTop.Size = New System.Drawing.Size(501, 244)
        Me.pnlLeftTop.TabIndex = 0
        '
        'pnlTopLeftTop
        '
        Me.pnlTopLeftTop.Controls.Add(Me.lblWelcome)
        Me.pnlTopLeftTop.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlTopLeftTop.Location = New System.Drawing.Point(0, 0)
        Me.pnlTopLeftTop.Name = "pnlTopLeftTop"
        Me.pnlTopLeftTop.Size = New System.Drawing.Size(501, 197)
        Me.pnlTopLeftTop.TabIndex = 1
        '
        'lblWelcome
        '
        Me.lblWelcome.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblWelcome.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWelcome.ForeColor = System.Drawing.Color.White
        Me.lblWelcome.Location = New System.Drawing.Point(0, 0)
        Me.lblWelcome.Name = "lblWelcome"
        Me.lblWelcome.Size = New System.Drawing.Size(501, 197)
        Me.lblWelcome.TabIndex = 0
        Me.lblWelcome.Text = "Welcome to the Show!"
        Me.lblWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblWelcome.UseCompatibleTextRendering = True
        '
        'pnlTopLeftBottom
        '
        Me.pnlTopLeftBottom.Controls.Add(Me.lblCounter)
        Me.pnlTopLeftBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlTopLeftBottom.Location = New System.Drawing.Point(0, 197)
        Me.pnlTopLeftBottom.Name = "pnlTopLeftBottom"
        Me.pnlTopLeftBottom.Size = New System.Drawing.Size(501, 47)
        Me.pnlTopLeftBottom.TabIndex = 0
        '
        'lblCounter
        '
        Me.lblCounter.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblCounter.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCounter.ForeColor = System.Drawing.Color.White
        Me.lblCounter.Location = New System.Drawing.Point(0, 0)
        Me.lblCounter.Name = "lblCounter"
        Me.lblCounter.Size = New System.Drawing.Size(501, 47)
        Me.lblCounter.TabIndex = 0
        Me.lblCounter.Text = "Total Contestants: 0"
        Me.lblCounter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblCounter.UseCompatibleTextRendering = True
        '
        'frmPresenter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Black
        Me.BackgroundImage = Global.RandomSelector.My.Resources.Resources.MillionaireSeason7BG
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1008, 537)
        Me.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Controls.Add(Me.pnlTopRow)
        Me.Controls.Add(Me.pnlActionFrame)
        Me.Controls.Add(Me.pnlContestantName)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmPresenter"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.Text = "Presenter"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.pnlContestantName.ResumeLayout(False)
        Me.pnlNameFrame.ResumeLayout(False)
        CType(Me.pbNameFrame, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlActionFrame.ResumeLayout(False)
        Me.pnlTopRow.ResumeLayout(False)
        Me.pnlRightTop.ResumeLayout(False)
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlLeftTop.ResumeLayout(False)
        Me.pnlTopLeftTop.ResumeLayout(False)
        Me.pnlTopLeftBottom.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents pnlContestantName As Panel
    Friend WithEvents pbNameFrame As PictureBox
    Friend WithEvents pnlActionFrame As Panel
    Friend WithEvents pnlNameFrame As Panel
    Friend WithEvents lblContestantName As Label
    Friend WithEvents miHideBorder As ToolStripMenuItem
    Friend WithEvents lblCallToAction As Label
    Friend WithEvents tmrContestantBlink As Timer
    Friend WithEvents pnlTopRow As Panel
    Friend WithEvents pnlRightTop As Panel
    Friend WithEvents pnlLeftTop As Panel
    Friend WithEvents pnlTopLeftTop As Panel
    Friend WithEvents pnlTopLeftBottom As Panel
    Friend WithEvents lblWelcome As Label
    Friend WithEvents lblCounter As Label
    Friend WithEvents pbLogo As PictureBox
End Class
