﻿Imports WMPLib

Public Class frmSoundOptions



    Private Sub btnBrowseBackgroundMusic_Click(sender As Object, e As EventArgs) Handles btnBrowseSelectionRoll.Click, btnBrowseDrumRoll.Click, btnBrowseBackgroundMusic.Click

        Dim clickedButton As Button = CType(sender, Button)

        If MusicFileDialog.ShowDialog() = DialogResult.OK Then
            SetTextBoxValue(clickedButton.Tag.ToString, MusicFileDialog.FileName)
        End If
    End Sub
    Private Sub SetTextBoxValue(ByVal fieldtag As String, ByVal input As String)

        ' Find the TextBox with the aforementioned "tag"
        Dim txtb = Controls.Find("txt" & fieldtag, True)
        If txtb.Count > 0 Then
            Dim txt As TextBox = txtb.First
            txt.Text = input.Trim
        End If

    End Sub

    Private Sub frmSoundOptions_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Track1.controls.stop()
        Track2.controls.stop()

        Track1 = Nothing
        Track2 = Nothing

        Me.DialogResult = DialogResult.OK
    End Sub
End Class