﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMainScreen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ofdLogoSearch = New System.Windows.Forms.OpenFileDialog()
        Me.mmMainMenu = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tmrClock = New System.Windows.Forms.Timer(Me.components)
        Me.tbController = New System.Windows.Forms.TabControl()
        Me.tbPool = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblPoolCount = New System.Windows.Forms.Label()
        Me.lblPoolCount_h = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblFileCount = New System.Windows.Forms.Label()
        Me.btnAddFromFile = New System.Windows.Forms.Button()
        Me.btnBrowseTextFile = New System.Windows.Forms.Button()
        Me.txtTextFileName = New System.Windows.Forms.TextBox()
        Me.grpAboutBox = New System.Windows.Forms.GroupBox()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grpSoundConfig = New System.Windows.Forms.GroupBox()
        Me.lblBackgroundMusic_h = New System.Windows.Forms.Label()
        Me.btnPreviewBackgroundMusic = New System.Windows.Forms.Button()
        Me.btnBrowseBackgroundMusic = New System.Windows.Forms.Button()
        Me.txtBackgroundMusic = New System.Windows.Forms.TextBox()
        Me.lblSelectionRoll_h = New System.Windows.Forms.Label()
        Me.btnPreviewSelectionRoll = New System.Windows.Forms.Button()
        Me.btnBrowseSelectionRoll = New System.Windows.Forms.Button()
        Me.txtSelectionRoll = New System.Windows.Forms.TextBox()
        Me.lblDrumRoll_h = New System.Windows.Forms.Label()
        Me.btnPreviewDrumRoll = New System.Windows.Forms.Button()
        Me.btnBrowseDrumRoll = New System.Windows.Forms.Button()
        Me.txtDrumRoll = New System.Windows.Forms.TextBox()
        Me.grpLogoPreview = New System.Windows.Forms.GroupBox()
        Me.pbPictureBox = New System.Windows.Forms.PictureBox()
        Me.grpLogoInfo = New System.Windows.Forms.GroupBox()
        Me.lblWelcomeText = New System.Windows.Forms.Label()
        Me.txtWelcomeText = New System.Windows.Forms.TextBox()
        Me.lblCallToAction = New System.Windows.Forms.Label()
        Me.txtCallToAction = New System.Windows.Forms.TextBox()
        Me.lblShowLogo_h = New System.Windows.Forms.Label()
        Me.btnBrowseLogo = New System.Windows.Forms.Button()
        Me.txtLogoFileName = New System.Windows.Forms.TextBox()
        Me.grpBatch = New System.Windows.Forms.GroupBox()
        Me.rbContestant = New System.Windows.Forms.RadioButton()
        Me.rbTicket = New System.Windows.Forms.RadioButton()
        Me.lblHighest_h = New System.Windows.Forms.Label()
        Me.lblLowest_h = New System.Windows.Forms.Label()
        Me.txtHighest = New System.Windows.Forms.TextBox()
        Me.txtLowest = New System.Windows.Forms.TextBox()
        Me.btnAddBatch = New System.Windows.Forms.Button()
        Me.grpSinglePlayer = New System.Windows.Forms.GroupBox()
        Me.btnAddContestant = New System.Windows.Forms.Button()
        Me.lblContestant_h = New System.Windows.Forms.Label()
        Me.txtContestantName = New System.Windows.Forms.TextBox()
        Me.tbContestant = New System.Windows.Forms.TabPage()
        Me.grpAudioControls = New System.Windows.Forms.GroupBox()
        Me.btnPlaySelection = New System.Windows.Forms.Button()
        Me.btnPlayDrums = New System.Windows.Forms.Button()
        Me.btnPlayBackground = New System.Windows.Forms.Button()
        Me.btnKillSounds = New System.Windows.Forms.Button()
        Me.grpLastContestant = New System.Windows.Forms.GroupBox()
        Me.btnResetWinner = New System.Windows.Forms.Button()
        Me.lblSelectedPlayer = New System.Windows.Forms.Label()
        Me.grpSelectedNames = New System.Windows.Forms.GroupBox()
        Me.dgvSelected = New System.Windows.Forms.DataGridView()
        Me.SelectedName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SelectedID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.grpControlPanel = New System.Windows.Forms.GroupBox()
        Me.cbFrameDisplay = New System.Windows.Forms.CheckBox()
        Me.btnShowPresenter = New System.Windows.Forms.Button()
        Me.btnClearWinners = New System.Windows.Forms.Button()
        Me.btnStartRandomizer = New System.Windows.Forms.Button()
        Me.cbClearSelected = New System.Windows.Forms.CheckBox()
        Me.btnStopRandomizer = New System.Windows.Forms.Button()
        Me.grpContestantPool = New System.Windows.Forms.GroupBox()
        Me.pnlDataGridView = New System.Windows.Forms.Panel()
        Me.dgvContestantPool = New System.Windows.Forms.DataGridView()
        Me.ContestantName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContestantID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlPoolControls = New System.Windows.Forms.Panel()
        Me.lblPlayerCount = New System.Windows.Forms.Label()
        Me.lblPlayerCount_h = New System.Windows.Forms.Label()
        Me.btnClearPool = New System.Windows.Forms.Button()
        Me.btnDeleteContestant = New System.Windows.Forms.Button()
        Me.MusicFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.ofdPlainText = New System.Windows.Forms.OpenFileDialog()
        Me.mmMainMenu.SuspendLayout()
        Me.tbController.SuspendLayout()
        Me.tbPool.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.grpAboutBox.SuspendLayout()
        Me.grpSoundConfig.SuspendLayout()
        Me.grpLogoPreview.SuspendLayout()
        CType(Me.pbPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpLogoInfo.SuspendLayout()
        Me.grpBatch.SuspendLayout()
        Me.grpSinglePlayer.SuspendLayout()
        Me.tbContestant.SuspendLayout()
        Me.grpAudioControls.SuspendLayout()
        Me.grpLastContestant.SuspendLayout()
        Me.grpSelectedNames.SuspendLayout()
        CType(Me.dgvSelected, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpControlPanel.SuspendLayout()
        Me.grpContestantPool.SuspendLayout()
        Me.pnlDataGridView.SuspendLayout()
        CType(Me.dgvContestantPool, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlPoolControls.SuspendLayout()
        Me.SuspendLayout()
        '
        'ofdLogoSearch
        '
        Me.ofdLogoSearch.Filter = "JPEG Image|*.jpg|GIF Image|*.gif|PNG Image|*.png|All Images|*.jpg;*.gif;*.png"
        Me.ofdLogoSearch.FilterIndex = 4
        Me.ofdLogoSearch.Title = "Choose an Image for the Thumbnail..."
        '
        'mmMainMenu
        '
        Me.mmMainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.mmMainMenu.Location = New System.Drawing.Point(0, 0)
        Me.mmMainMenu.Name = "mmMainMenu"
        Me.mmMainMenu.Size = New System.Drawing.Size(1129, 24)
        Me.mmMainMenu.TabIndex = 1
        Me.mmMainMenu.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'tmrClock
        '
        Me.tmrClock.Interval = 38
        '
        'tbController
        '
        Me.tbController.Controls.Add(Me.tbPool)
        Me.tbController.Controls.Add(Me.tbContestant)
        Me.tbController.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbController.Location = New System.Drawing.Point(0, 24)
        Me.tbController.Name = "tbController"
        Me.tbController.SelectedIndex = 0
        Me.tbController.Size = New System.Drawing.Size(1129, 574)
        Me.tbController.TabIndex = 0
        '
        'tbPool
        '
        Me.tbPool.BackColor = System.Drawing.SystemColors.Control
        Me.tbPool.Controls.Add(Me.GroupBox2)
        Me.tbPool.Controls.Add(Me.GroupBox1)
        Me.tbPool.Controls.Add(Me.grpAboutBox)
        Me.tbPool.Controls.Add(Me.grpSoundConfig)
        Me.tbPool.Controls.Add(Me.grpLogoPreview)
        Me.tbPool.Controls.Add(Me.grpLogoInfo)
        Me.tbPool.Controls.Add(Me.grpBatch)
        Me.tbPool.Controls.Add(Me.grpSinglePlayer)
        Me.tbPool.Location = New System.Drawing.Point(4, 22)
        Me.tbPool.Name = "tbPool"
        Me.tbPool.Padding = New System.Windows.Forms.Padding(3)
        Me.tbPool.Size = New System.Drawing.Size(1121, 548)
        Me.tbPool.TabIndex = 0
        Me.tbPool.Text = "Pool Setup"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblPoolCount)
        Me.GroupBox2.Controls.Add(Me.lblPoolCount_h)
        Me.GroupBox2.Location = New System.Drawing.Point(648, 489)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(457, 51)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Contestant Pool Size"
        '
        'lblPoolCount
        '
        Me.lblPoolCount.AutoSize = True
        Me.lblPoolCount.Location = New System.Drawing.Point(276, 24)
        Me.lblPoolCount.Name = "lblPoolCount"
        Me.lblPoolCount.Size = New System.Drawing.Size(13, 13)
        Me.lblPoolCount.TabIndex = 1
        Me.lblPoolCount.Text = "0"
        '
        'lblPoolCount_h
        '
        Me.lblPoolCount_h.AutoSize = True
        Me.lblPoolCount_h.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPoolCount_h.Location = New System.Drawing.Point(167, 24)
        Me.lblPoolCount_h.Name = "lblPoolCount_h"
        Me.lblPoolCount_h.Size = New System.Drawing.Size(106, 13)
        Me.lblPoolCount_h.TabIndex = 0
        Me.lblPoolCount_h.Text = "Total Pool Count:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblFileCount)
        Me.GroupBox1.Controls.Add(Me.btnAddFromFile)
        Me.GroupBox1.Controls.Add(Me.btnBrowseTextFile)
        Me.GroupBox1.Controls.Add(Me.txtTextFileName)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 195)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(636, 85)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Add From File"
        '
        'lblFileCount
        '
        Me.lblFileCount.Location = New System.Drawing.Point(459, 58)
        Me.lblFileCount.Name = "lblFileCount"
        Me.lblFileCount.Size = New System.Drawing.Size(171, 16)
        Me.lblFileCount.TabIndex = 3
        Me.lblFileCount.Text = "Empty File"
        Me.lblFileCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnAddFromFile
        '
        Me.btnAddFromFile.Enabled = False
        Me.btnAddFromFile.Location = New System.Drawing.Point(459, 30)
        Me.btnAddFromFile.Name = "btnAddFromFile"
        Me.btnAddFromFile.Size = New System.Drawing.Size(171, 26)
        Me.btnAddFromFile.TabIndex = 2
        Me.btnAddFromFile.Text = "Add Players From File"
        Me.btnAddFromFile.UseVisualStyleBackColor = True
        '
        'btnBrowseTextFile
        '
        Me.btnBrowseTextFile.Location = New System.Drawing.Point(324, 30)
        Me.btnBrowseTextFile.Name = "btnBrowseTextFile"
        Me.btnBrowseTextFile.Size = New System.Drawing.Size(129, 39)
        Me.btnBrowseTextFile.TabIndex = 1
        Me.btnBrowseTextFile.Text = "Browse For Text File"
        Me.btnBrowseTextFile.UseVisualStyleBackColor = True
        '
        'txtTextFileName
        '
        Me.txtTextFileName.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTextFileName.Location = New System.Drawing.Point(9, 35)
        Me.txtTextFileName.MaxLength = 30
        Me.txtTextFileName.Name = "txtTextFileName"
        Me.txtTextFileName.Size = New System.Drawing.Size(309, 29)
        Me.txtTextFileName.TabIndex = 0
        '
        'grpAboutBox
        '
        Me.grpAboutBox.Controls.Add(Me.lblTitle)
        Me.grpAboutBox.Controls.Add(Me.Label1)
        Me.grpAboutBox.Location = New System.Drawing.Point(6, 374)
        Me.grpAboutBox.Name = "grpAboutBox"
        Me.grpAboutBox.Size = New System.Drawing.Size(636, 166)
        Me.grpAboutBox.TabIndex = 4
        Me.grpAboutBox.TabStop = False
        Me.grpAboutBox.Text = "About This Program"
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitle.Location = New System.Drawing.Point(204, 59)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(249, 24)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "Come On Down Picker v1"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(236, 95)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(175, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "By  :  Paul Williams && Scott Goldiner"
        '
        'grpSoundConfig
        '
        Me.grpSoundConfig.Controls.Add(Me.lblBackgroundMusic_h)
        Me.grpSoundConfig.Controls.Add(Me.btnPreviewBackgroundMusic)
        Me.grpSoundConfig.Controls.Add(Me.btnBrowseBackgroundMusic)
        Me.grpSoundConfig.Controls.Add(Me.txtBackgroundMusic)
        Me.grpSoundConfig.Controls.Add(Me.lblSelectionRoll_h)
        Me.grpSoundConfig.Controls.Add(Me.btnPreviewSelectionRoll)
        Me.grpSoundConfig.Controls.Add(Me.btnBrowseSelectionRoll)
        Me.grpSoundConfig.Controls.Add(Me.txtSelectionRoll)
        Me.grpSoundConfig.Controls.Add(Me.lblDrumRoll_h)
        Me.grpSoundConfig.Controls.Add(Me.btnPreviewDrumRoll)
        Me.grpSoundConfig.Controls.Add(Me.btnBrowseDrumRoll)
        Me.grpSoundConfig.Controls.Add(Me.txtDrumRoll)
        Me.grpSoundConfig.Location = New System.Drawing.Point(648, 315)
        Me.grpSoundConfig.Name = "grpSoundConfig"
        Me.grpSoundConfig.Size = New System.Drawing.Size(457, 168)
        Me.grpSoundConfig.TabIndex = 5
        Me.grpSoundConfig.TabStop = False
        Me.grpSoundConfig.Text = "Sound Configuration"
        '
        'lblBackgroundMusic_h
        '
        Me.lblBackgroundMusic_h.AutoSize = True
        Me.lblBackgroundMusic_h.Location = New System.Drawing.Point(12, 129)
        Me.lblBackgroundMusic_h.Name = "lblBackgroundMusic_h"
        Me.lblBackgroundMusic_h.Size = New System.Drawing.Size(96, 13)
        Me.lblBackgroundMusic_h.TabIndex = 8
        Me.lblBackgroundMusic_h.Text = "Background Music"
        '
        'btnPreviewBackgroundMusic
        '
        Me.btnPreviewBackgroundMusic.Image = Global.RandomSelector.My.Resources.Resources.small_play_button
        Me.btnPreviewBackgroundMusic.Location = New System.Drawing.Point(417, 121)
        Me.btnPreviewBackgroundMusic.Name = "btnPreviewBackgroundMusic"
        Me.btnPreviewBackgroundMusic.Size = New System.Drawing.Size(28, 23)
        Me.btnPreviewBackgroundMusic.TabIndex = 11
        Me.btnPreviewBackgroundMusic.Tag = "BackgroundMusic"
        Me.btnPreviewBackgroundMusic.UseVisualStyleBackColor = True
        '
        'btnBrowseBackgroundMusic
        '
        Me.btnBrowseBackgroundMusic.Location = New System.Drawing.Point(336, 121)
        Me.btnBrowseBackgroundMusic.Name = "btnBrowseBackgroundMusic"
        Me.btnBrowseBackgroundMusic.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowseBackgroundMusic.TabIndex = 10
        Me.btnBrowseBackgroundMusic.Tag = "BackgroundMusic"
        Me.btnBrowseBackgroundMusic.Text = "Browse..."
        Me.btnBrowseBackgroundMusic.UseVisualStyleBackColor = True
        '
        'txtBackgroundMusic
        '
        Me.txtBackgroundMusic.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBackgroundMusic.Location = New System.Drawing.Point(114, 118)
        Me.txtBackgroundMusic.Name = "txtBackgroundMusic"
        Me.txtBackgroundMusic.Size = New System.Drawing.Size(216, 29)
        Me.txtBackgroundMusic.TabIndex = 9
        Me.txtBackgroundMusic.Tag = "BackgroundMusic"
        '
        'lblSelectionRoll_h
        '
        Me.lblSelectionRoll_h.AutoSize = True
        Me.lblSelectionRoll_h.Location = New System.Drawing.Point(12, 78)
        Me.lblSelectionRoll_h.Name = "lblSelectionRoll_h"
        Me.lblSelectionRoll_h.Size = New System.Drawing.Size(72, 13)
        Me.lblSelectionRoll_h.TabIndex = 4
        Me.lblSelectionRoll_h.Text = "Selection Roll"
        '
        'btnPreviewSelectionRoll
        '
        Me.btnPreviewSelectionRoll.Image = Global.RandomSelector.My.Resources.Resources.small_play_button
        Me.btnPreviewSelectionRoll.Location = New System.Drawing.Point(417, 73)
        Me.btnPreviewSelectionRoll.Name = "btnPreviewSelectionRoll"
        Me.btnPreviewSelectionRoll.Size = New System.Drawing.Size(28, 23)
        Me.btnPreviewSelectionRoll.TabIndex = 7
        Me.btnPreviewSelectionRoll.Tag = "SelectionRoll"
        Me.btnPreviewSelectionRoll.UseVisualStyleBackColor = True
        '
        'btnBrowseSelectionRoll
        '
        Me.btnBrowseSelectionRoll.Location = New System.Drawing.Point(336, 73)
        Me.btnBrowseSelectionRoll.Name = "btnBrowseSelectionRoll"
        Me.btnBrowseSelectionRoll.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowseSelectionRoll.TabIndex = 6
        Me.btnBrowseSelectionRoll.Tag = "SelectionRoll"
        Me.btnBrowseSelectionRoll.Text = "Browse..."
        Me.btnBrowseSelectionRoll.UseVisualStyleBackColor = True
        '
        'txtSelectionRoll
        '
        Me.txtSelectionRoll.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSelectionRoll.Location = New System.Drawing.Point(114, 70)
        Me.txtSelectionRoll.Name = "txtSelectionRoll"
        Me.txtSelectionRoll.Size = New System.Drawing.Size(216, 29)
        Me.txtSelectionRoll.TabIndex = 5
        Me.txtSelectionRoll.Tag = "SelectionRoll"
        '
        'lblDrumRoll_h
        '
        Me.lblDrumRoll_h.AutoSize = True
        Me.lblDrumRoll_h.Location = New System.Drawing.Point(12, 29)
        Me.lblDrumRoll_h.Name = "lblDrumRoll_h"
        Me.lblDrumRoll_h.Size = New System.Drawing.Size(53, 13)
        Me.lblDrumRoll_h.TabIndex = 0
        Me.lblDrumRoll_h.Text = "Drum Roll"
        '
        'btnPreviewDrumRoll
        '
        Me.btnPreviewDrumRoll.Image = Global.RandomSelector.My.Resources.Resources.small_play_button
        Me.btnPreviewDrumRoll.Location = New System.Drawing.Point(417, 24)
        Me.btnPreviewDrumRoll.Name = "btnPreviewDrumRoll"
        Me.btnPreviewDrumRoll.Size = New System.Drawing.Size(28, 23)
        Me.btnPreviewDrumRoll.TabIndex = 3
        Me.btnPreviewDrumRoll.Tag = "DrumRoll"
        Me.btnPreviewDrumRoll.UseVisualStyleBackColor = True
        '
        'btnBrowseDrumRoll
        '
        Me.btnBrowseDrumRoll.Location = New System.Drawing.Point(336, 24)
        Me.btnBrowseDrumRoll.Name = "btnBrowseDrumRoll"
        Me.btnBrowseDrumRoll.Size = New System.Drawing.Size(75, 23)
        Me.btnBrowseDrumRoll.TabIndex = 2
        Me.btnBrowseDrumRoll.Tag = "DrumRoll"
        Me.btnBrowseDrumRoll.Text = "Browse..."
        Me.btnBrowseDrumRoll.UseVisualStyleBackColor = True
        '
        'txtDrumRoll
        '
        Me.txtDrumRoll.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDrumRoll.Location = New System.Drawing.Point(114, 21)
        Me.txtDrumRoll.Name = "txtDrumRoll"
        Me.txtDrumRoll.Size = New System.Drawing.Size(216, 29)
        Me.txtDrumRoll.TabIndex = 1
        Me.txtDrumRoll.Tag = "DrumRoll"
        '
        'grpLogoPreview
        '
        Me.grpLogoPreview.Controls.Add(Me.pbPictureBox)
        Me.grpLogoPreview.Location = New System.Drawing.Point(648, 6)
        Me.grpLogoPreview.Name = "grpLogoPreview"
        Me.grpLogoPreview.Size = New System.Drawing.Size(457, 303)
        Me.grpLogoPreview.TabIndex = 0
        Me.grpLogoPreview.TabStop = False
        Me.grpLogoPreview.Text = "Logo Preview"
        '
        'pbPictureBox
        '
        Me.pbPictureBox.Location = New System.Drawing.Point(92, 13)
        Me.pbPictureBox.Name = "pbPictureBox"
        Me.pbPictureBox.Size = New System.Drawing.Size(272, 272)
        Me.pbPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbPictureBox.TabIndex = 9
        Me.pbPictureBox.TabStop = False
        '
        'grpLogoInfo
        '
        Me.grpLogoInfo.Controls.Add(Me.lblWelcomeText)
        Me.grpLogoInfo.Controls.Add(Me.txtWelcomeText)
        Me.grpLogoInfo.Controls.Add(Me.lblCallToAction)
        Me.grpLogoInfo.Controls.Add(Me.txtCallToAction)
        Me.grpLogoInfo.Controls.Add(Me.lblShowLogo_h)
        Me.grpLogoInfo.Controls.Add(Me.btnBrowseLogo)
        Me.grpLogoInfo.Controls.Add(Me.txtLogoFileName)
        Me.grpLogoInfo.Location = New System.Drawing.Point(6, 286)
        Me.grpLogoInfo.Name = "grpLogoInfo"
        Me.grpLogoInfo.Size = New System.Drawing.Size(636, 82)
        Me.grpLogoInfo.TabIndex = 3
        Me.grpLogoInfo.TabStop = False
        Me.grpLogoInfo.Text = "Show Details"
        '
        'lblWelcomeText
        '
        Me.lblWelcomeText.AutoSize = True
        Me.lblWelcomeText.Location = New System.Drawing.Point(313, 21)
        Me.lblWelcomeText.Name = "lblWelcomeText"
        Me.lblWelcomeText.Size = New System.Drawing.Size(76, 13)
        Me.lblWelcomeText.TabIndex = 3
        Me.lblWelcomeText.Text = "Welcome Text"
        '
        'txtWelcomeText
        '
        Me.txtWelcomeText.Location = New System.Drawing.Point(399, 18)
        Me.txtWelcomeText.MaxLength = 100
        Me.txtWelcomeText.Name = "txtWelcomeText"
        Me.txtWelcomeText.Size = New System.Drawing.Size(170, 20)
        Me.txtWelcomeText.TabIndex = 4
        Me.txtWelcomeText.Text = "Welcome To The Show!"
        '
        'lblCallToAction
        '
        Me.lblCallToAction.AutoSize = True
        Me.lblCallToAction.Location = New System.Drawing.Point(313, 47)
        Me.lblCallToAction.Name = "lblCallToAction"
        Me.lblCallToAction.Size = New System.Drawing.Size(73, 13)
        Me.lblCallToAction.TabIndex = 5
        Me.lblCallToAction.Text = "Call To Action"
        '
        'txtCallToAction
        '
        Me.txtCallToAction.Location = New System.Drawing.Point(399, 44)
        Me.txtCallToAction.MaxLength = 200
        Me.txtCallToAction.Name = "txtCallToAction"
        Me.txtCallToAction.Size = New System.Drawing.Size(170, 20)
        Me.txtCallToAction.TabIndex = 6
        Me.txtCallToAction.Text = "Be sure to get a ticket if you would like to play!"
        '
        'lblShowLogo_h
        '
        Me.lblShowLogo_h.AutoSize = True
        Me.lblShowLogo_h.Location = New System.Drawing.Point(6, 25)
        Me.lblShowLogo_h.Name = "lblShowLogo_h"
        Me.lblShowLogo_h.Size = New System.Drawing.Size(61, 13)
        Me.lblShowLogo_h.TabIndex = 0
        Me.lblShowLogo_h.Text = "Show Logo"
        '
        'btnBrowseLogo
        '
        Me.btnBrowseLogo.Location = New System.Drawing.Point(125, 48)
        Me.btnBrowseLogo.Name = "btnBrowseLogo"
        Me.btnBrowseLogo.Size = New System.Drawing.Size(80, 20)
        Me.btnBrowseLogo.TabIndex = 2
        Me.btnBrowseLogo.Text = "Browse..."
        Me.btnBrowseLogo.UseVisualStyleBackColor = True
        '
        'txtLogoFileName
        '
        Me.txtLogoFileName.Location = New System.Drawing.Point(92, 22)
        Me.txtLogoFileName.Name = "txtLogoFileName"
        Me.txtLogoFileName.Size = New System.Drawing.Size(170, 20)
        Me.txtLogoFileName.TabIndex = 1
        '
        'grpBatch
        '
        Me.grpBatch.Controls.Add(Me.rbContestant)
        Me.grpBatch.Controls.Add(Me.rbTicket)
        Me.grpBatch.Controls.Add(Me.lblHighest_h)
        Me.grpBatch.Controls.Add(Me.lblLowest_h)
        Me.grpBatch.Controls.Add(Me.txtHighest)
        Me.grpBatch.Controls.Add(Me.txtLowest)
        Me.grpBatch.Controls.Add(Me.btnAddBatch)
        Me.grpBatch.Location = New System.Drawing.Point(6, 107)
        Me.grpBatch.Name = "grpBatch"
        Me.grpBatch.Size = New System.Drawing.Size(636, 82)
        Me.grpBatch.TabIndex = 1
        Me.grpBatch.TabStop = False
        Me.grpBatch.Text = "Add Batch of Contestants To Pool"
        '
        'rbContestant
        '
        Me.rbContestant.AutoSize = True
        Me.rbContestant.Location = New System.Drawing.Point(29, 44)
        Me.rbContestant.Name = "rbContestant"
        Me.rbContestant.Size = New System.Drawing.Size(147, 17)
        Me.rbContestant.TabIndex = 1
        Me.rbContestant.Text = "Prefix with ""Contestant #"""
        Me.rbContestant.UseVisualStyleBackColor = True
        '
        'rbTicket
        '
        Me.rbTicket.AutoSize = True
        Me.rbTicket.Checked = True
        Me.rbTicket.Location = New System.Drawing.Point(29, 22)
        Me.rbTicket.Name = "rbTicket"
        Me.rbTicket.Size = New System.Drawing.Size(126, 17)
        Me.rbTicket.TabIndex = 0
        Me.rbTicket.TabStop = True
        Me.rbTicket.Text = "Prefix with ""Ticket #"""
        Me.rbTicket.UseVisualStyleBackColor = True
        '
        'lblHighest_h
        '
        Me.lblHighest_h.AutoSize = True
        Me.lblHighest_h.Location = New System.Drawing.Point(216, 47)
        Me.lblHighest_h.Name = "lblHighest_h"
        Me.lblHighest_h.Size = New System.Drawing.Size(83, 13)
        Me.lblHighest_h.TabIndex = 4
        Me.lblHighest_h.Text = "Highest Number"
        '
        'lblLowest_h
        '
        Me.lblLowest_h.AutoSize = True
        Me.lblLowest_h.Location = New System.Drawing.Point(216, 21)
        Me.lblLowest_h.Name = "lblLowest_h"
        Me.lblLowest_h.Size = New System.Drawing.Size(81, 13)
        Me.lblLowest_h.TabIndex = 2
        Me.lblLowest_h.Text = "Lowest Number"
        '
        'txtHighest
        '
        Me.txtHighest.Location = New System.Drawing.Point(303, 44)
        Me.txtHighest.Name = "txtHighest"
        Me.txtHighest.Size = New System.Drawing.Size(110, 20)
        Me.txtHighest.TabIndex = 5
        '
        'txtLowest
        '
        Me.txtLowest.Location = New System.Drawing.Point(303, 18)
        Me.txtLowest.Name = "txtLowest"
        Me.txtLowest.Size = New System.Drawing.Size(110, 20)
        Me.txtLowest.TabIndex = 3
        '
        'btnAddBatch
        '
        Me.btnAddBatch.Enabled = False
        Me.btnAddBatch.Location = New System.Drawing.Point(457, 22)
        Me.btnAddBatch.Name = "btnAddBatch"
        Me.btnAddBatch.Size = New System.Drawing.Size(129, 39)
        Me.btnAddBatch.TabIndex = 6
        Me.btnAddBatch.Text = "Add Contestants To Pool"
        Me.btnAddBatch.UseVisualStyleBackColor = True
        '
        'grpSinglePlayer
        '
        Me.grpSinglePlayer.Controls.Add(Me.btnAddContestant)
        Me.grpSinglePlayer.Controls.Add(Me.lblContestant_h)
        Me.grpSinglePlayer.Controls.Add(Me.txtContestantName)
        Me.grpSinglePlayer.Location = New System.Drawing.Point(6, 19)
        Me.grpSinglePlayer.Name = "grpSinglePlayer"
        Me.grpSinglePlayer.Size = New System.Drawing.Size(636, 82)
        Me.grpSinglePlayer.TabIndex = 0
        Me.grpSinglePlayer.TabStop = False
        Me.grpSinglePlayer.Text = "Add Single Player To Pool"
        '
        'btnAddContestant
        '
        Me.btnAddContestant.Location = New System.Drawing.Point(457, 22)
        Me.btnAddContestant.Name = "btnAddContestant"
        Me.btnAddContestant.Size = New System.Drawing.Size(129, 39)
        Me.btnAddContestant.TabIndex = 2
        Me.btnAddContestant.Text = "Add Contestant To Pool"
        Me.btnAddContestant.UseVisualStyleBackColor = True
        '
        'lblContestant_h
        '
        Me.lblContestant_h.AutoSize = True
        Me.lblContestant_h.Location = New System.Drawing.Point(50, 35)
        Me.lblContestant_h.Name = "lblContestant_h"
        Me.lblContestant_h.Size = New System.Drawing.Size(89, 13)
        Me.lblContestant_h.TabIndex = 0
        Me.lblContestant_h.Text = "Contestant Name"
        '
        'txtContestantName
        '
        Me.txtContestantName.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtContestantName.Location = New System.Drawing.Point(164, 27)
        Me.txtContestantName.MaxLength = 30
        Me.txtContestantName.Name = "txtContestantName"
        Me.txtContestantName.Size = New System.Drawing.Size(261, 29)
        Me.txtContestantName.TabIndex = 1
        '
        'tbContestant
        '
        Me.tbContestant.BackColor = System.Drawing.SystemColors.Control
        Me.tbContestant.Controls.Add(Me.grpAudioControls)
        Me.tbContestant.Controls.Add(Me.grpLastContestant)
        Me.tbContestant.Controls.Add(Me.grpSelectedNames)
        Me.tbContestant.Controls.Add(Me.grpControlPanel)
        Me.tbContestant.Controls.Add(Me.grpContestantPool)
        Me.tbContestant.Location = New System.Drawing.Point(4, 22)
        Me.tbContestant.Name = "tbContestant"
        Me.tbContestant.Padding = New System.Windows.Forms.Padding(3)
        Me.tbContestant.Size = New System.Drawing.Size(1121, 548)
        Me.tbContestant.TabIndex = 1
        Me.tbContestant.Text = "Contestant Selection"
        '
        'grpAudioControls
        '
        Me.grpAudioControls.Controls.Add(Me.btnPlaySelection)
        Me.grpAudioControls.Controls.Add(Me.btnPlayDrums)
        Me.grpAudioControls.Controls.Add(Me.btnPlayBackground)
        Me.grpAudioControls.Controls.Add(Me.btnKillSounds)
        Me.grpAudioControls.Location = New System.Drawing.Point(608, 410)
        Me.grpAudioControls.Name = "grpAudioControls"
        Me.grpAudioControls.Size = New System.Drawing.Size(508, 74)
        Me.grpAudioControls.TabIndex = 4
        Me.grpAudioControls.TabStop = False
        Me.grpAudioControls.Text = "Audio Controls"
        '
        'btnPlaySelection
        '
        Me.btnPlaySelection.Location = New System.Drawing.Point(324, 12)
        Me.btnPlaySelection.Name = "btnPlaySelection"
        Me.btnPlaySelection.Size = New System.Drawing.Size(128, 27)
        Me.btnPlaySelection.TabIndex = 2
        Me.btnPlaySelection.Text = "Selection Roll"
        Me.btnPlaySelection.UseVisualStyleBackColor = True
        '
        'btnPlayDrums
        '
        Me.btnPlayDrums.Location = New System.Drawing.Point(190, 12)
        Me.btnPlayDrums.Name = "btnPlayDrums"
        Me.btnPlayDrums.Size = New System.Drawing.Size(128, 27)
        Me.btnPlayDrums.TabIndex = 1
        Me.btnPlayDrums.Text = "Drum Roll"
        Me.btnPlayDrums.UseVisualStyleBackColor = True
        '
        'btnPlayBackground
        '
        Me.btnPlayBackground.Location = New System.Drawing.Point(56, 12)
        Me.btnPlayBackground.Name = "btnPlayBackground"
        Me.btnPlayBackground.Size = New System.Drawing.Size(128, 27)
        Me.btnPlayBackground.TabIndex = 0
        Me.btnPlayBackground.Text = "Background "
        Me.btnPlayBackground.UseVisualStyleBackColor = True
        '
        'btnKillSounds
        '
        Me.btnKillSounds.BackColor = System.Drawing.Color.Red
        Me.btnKillSounds.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnKillSounds.ForeColor = System.Drawing.Color.White
        Me.btnKillSounds.Location = New System.Drawing.Point(190, 41)
        Me.btnKillSounds.Name = "btnKillSounds"
        Me.btnKillSounds.Size = New System.Drawing.Size(128, 27)
        Me.btnKillSounds.TabIndex = 3
        Me.btnKillSounds.Text = "KILL ALL SOUNDS"
        Me.btnKillSounds.UseVisualStyleBackColor = False
        '
        'grpLastContestant
        '
        Me.grpLastContestant.Controls.Add(Me.btnResetWinner)
        Me.grpLastContestant.Controls.Add(Me.lblSelectedPlayer)
        Me.grpLastContestant.Location = New System.Drawing.Point(605, 315)
        Me.grpLastContestant.Name = "grpLastContestant"
        Me.grpLastContestant.Size = New System.Drawing.Size(508, 89)
        Me.grpLastContestant.TabIndex = 3
        Me.grpLastContestant.TabStop = False
        Me.grpLastContestant.Text = "Last Selected Contestant"
        '
        'btnResetWinner
        '
        Me.btnResetWinner.Location = New System.Drawing.Point(423, 60)
        Me.btnResetWinner.Name = "btnResetWinner"
        Me.btnResetWinner.Size = New System.Drawing.Size(75, 23)
        Me.btnResetWinner.TabIndex = 1
        Me.btnResetWinner.Text = "Reset"
        Me.btnResetWinner.UseVisualStyleBackColor = True
        Me.btnResetWinner.Visible = False
        '
        'lblSelectedPlayer
        '
        Me.lblSelectedPlayer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblSelectedPlayer.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectedPlayer.Location = New System.Drawing.Point(3, 16)
        Me.lblSelectedPlayer.Name = "lblSelectedPlayer"
        Me.lblSelectedPlayer.Size = New System.Drawing.Size(502, 70)
        Me.lblSelectedPlayer.TabIndex = 0
        Me.lblSelectedPlayer.Text = "Waiting to choose..."
        Me.lblSelectedPlayer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grpSelectedNames
        '
        Me.grpSelectedNames.Controls.Add(Me.dgvSelected)
        Me.grpSelectedNames.Location = New System.Drawing.Point(608, 6)
        Me.grpSelectedNames.Name = "grpSelectedNames"
        Me.grpSelectedNames.Size = New System.Drawing.Size(505, 303)
        Me.grpSelectedNames.TabIndex = 2
        Me.grpSelectedNames.TabStop = False
        Me.grpSelectedNames.Text = "Selected Contestants"
        '
        'dgvSelected
        '
        Me.dgvSelected.AllowUserToAddRows = False
        Me.dgvSelected.AllowUserToDeleteRows = False
        Me.dgvSelected.AllowUserToResizeColumns = False
        Me.dgvSelected.AllowUserToResizeRows = False
        Me.dgvSelected.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvSelected.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSelected.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SelectedName, Me.SelectedID})
        Me.dgvSelected.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvSelected.Location = New System.Drawing.Point(3, 16)
        Me.dgvSelected.Name = "dgvSelected"
        Me.dgvSelected.ReadOnly = True
        Me.dgvSelected.RowHeadersVisible = False
        Me.dgvSelected.Size = New System.Drawing.Size(499, 284)
        Me.dgvSelected.TabIndex = 0
        '
        'SelectedName
        '
        Me.SelectedName.HeaderText = "Selected Player Name"
        Me.SelectedName.Name = "SelectedName"
        Me.SelectedName.ReadOnly = True
        '
        'SelectedID
        '
        Me.SelectedID.HeaderText = "Selected Player ID"
        Me.SelectedID.Name = "SelectedID"
        Me.SelectedID.ReadOnly = True
        Me.SelectedID.Visible = False
        '
        'grpControlPanel
        '
        Me.grpControlPanel.Controls.Add(Me.cbFrameDisplay)
        Me.grpControlPanel.Controls.Add(Me.btnShowPresenter)
        Me.grpControlPanel.Controls.Add(Me.btnClearWinners)
        Me.grpControlPanel.Controls.Add(Me.btnStartRandomizer)
        Me.grpControlPanel.Controls.Add(Me.cbClearSelected)
        Me.grpControlPanel.Controls.Add(Me.btnStopRandomizer)
        Me.grpControlPanel.Location = New System.Drawing.Point(11, 315)
        Me.grpControlPanel.Name = "grpControlPanel"
        Me.grpControlPanel.Size = New System.Drawing.Size(591, 86)
        Me.grpControlPanel.TabIndex = 1
        Me.grpControlPanel.TabStop = False
        Me.grpControlPanel.Text = "Control Panel"
        '
        'cbFrameDisplay
        '
        Me.cbFrameDisplay.AutoSize = True
        Me.cbFrameDisplay.Checked = True
        Me.cbFrameDisplay.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbFrameDisplay.Location = New System.Drawing.Point(442, 55)
        Me.cbFrameDisplay.Name = "cbFrameDisplay"
        Me.cbFrameDisplay.Size = New System.Drawing.Size(85, 17)
        Me.cbFrameDisplay.TabIndex = 5
        Me.cbFrameDisplay.Text = "Show Frame"
        Me.cbFrameDisplay.UseVisualStyleBackColor = True
        '
        'btnShowPresenter
        '
        Me.btnShowPresenter.Location = New System.Drawing.Point(424, 14)
        Me.btnShowPresenter.Name = "btnShowPresenter"
        Me.btnShowPresenter.Size = New System.Drawing.Size(120, 34)
        Me.btnShowPresenter.TabIndex = 4
        Me.btnShowPresenter.Text = "Show Presenter"
        Me.btnShowPresenter.UseVisualStyleBackColor = True
        '
        'btnClearWinners
        '
        Me.btnClearWinners.Location = New System.Drawing.Point(298, 14)
        Me.btnClearWinners.Name = "btnClearWinners"
        Me.btnClearWinners.Size = New System.Drawing.Size(120, 34)
        Me.btnClearWinners.TabIndex = 3
        Me.btnClearWinners.Text = "Clear Winner List"
        Me.btnClearWinners.UseVisualStyleBackColor = True
        '
        'btnStartRandomizer
        '
        Me.btnStartRandomizer.Enabled = False
        Me.btnStartRandomizer.Location = New System.Drawing.Point(46, 14)
        Me.btnStartRandomizer.Name = "btnStartRandomizer"
        Me.btnStartRandomizer.Size = New System.Drawing.Size(120, 34)
        Me.btnStartRandomizer.TabIndex = 0
        Me.btnStartRandomizer.Text = "Start Randomizer"
        Me.btnStartRandomizer.UseVisualStyleBackColor = True
        '
        'cbClearSelected
        '
        Me.cbClearSelected.AutoSize = True
        Me.cbClearSelected.Checked = True
        Me.cbClearSelected.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbClearSelected.Location = New System.Drawing.Point(161, 56)
        Me.cbClearSelected.Name = "cbClearSelected"
        Me.cbClearSelected.Size = New System.Drawing.Size(142, 17)
        Me.cbClearSelected.TabIndex = 2
        Me.cbClearSelected.Text = "Clear Selected from Pool"
        Me.cbClearSelected.UseVisualStyleBackColor = True
        '
        'btnStopRandomizer
        '
        Me.btnStopRandomizer.Enabled = False
        Me.btnStopRandomizer.Location = New System.Drawing.Point(172, 14)
        Me.btnStopRandomizer.Name = "btnStopRandomizer"
        Me.btnStopRandomizer.Size = New System.Drawing.Size(120, 34)
        Me.btnStopRandomizer.TabIndex = 1
        Me.btnStopRandomizer.Text = "Stop Randomizer"
        Me.btnStopRandomizer.UseVisualStyleBackColor = True
        '
        'grpContestantPool
        '
        Me.grpContestantPool.Controls.Add(Me.pnlDataGridView)
        Me.grpContestantPool.Controls.Add(Me.pnlPoolControls)
        Me.grpContestantPool.Location = New System.Drawing.Point(8, 6)
        Me.grpContestantPool.Name = "grpContestantPool"
        Me.grpContestantPool.Size = New System.Drawing.Size(594, 303)
        Me.grpContestantPool.TabIndex = 0
        Me.grpContestantPool.TabStop = False
        Me.grpContestantPool.Text = "Contestant Pool"
        '
        'pnlDataGridView
        '
        Me.pnlDataGridView.Controls.Add(Me.dgvContestantPool)
        Me.pnlDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlDataGridView.Location = New System.Drawing.Point(3, 16)
        Me.pnlDataGridView.Name = "pnlDataGridView"
        Me.pnlDataGridView.Size = New System.Drawing.Size(588, 224)
        Me.pnlDataGridView.TabIndex = 2
        '
        'dgvContestantPool
        '
        Me.dgvContestantPool.AllowUserToAddRows = False
        Me.dgvContestantPool.AllowUserToDeleteRows = False
        Me.dgvContestantPool.AllowUserToResizeColumns = False
        Me.dgvContestantPool.AllowUserToResizeRows = False
        Me.dgvContestantPool.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvContestantPool.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvContestantPool.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ContestantName, Me.ContestantID})
        Me.dgvContestantPool.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvContestantPool.Location = New System.Drawing.Point(0, 0)
        Me.dgvContestantPool.Name = "dgvContestantPool"
        Me.dgvContestantPool.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvContestantPool.Size = New System.Drawing.Size(588, 224)
        Me.dgvContestantPool.TabIndex = 0
        '
        'ContestantName
        '
        Me.ContestantName.HeaderText = "Contestant Name"
        Me.ContestantName.Name = "ContestantName"
        '
        'ContestantID
        '
        Me.ContestantID.HeaderText = "ID#"
        Me.ContestantID.Name = "ContestantID"
        Me.ContestantID.Visible = False
        '
        'pnlPoolControls
        '
        Me.pnlPoolControls.Controls.Add(Me.lblPlayerCount)
        Me.pnlPoolControls.Controls.Add(Me.lblPlayerCount_h)
        Me.pnlPoolControls.Controls.Add(Me.btnClearPool)
        Me.pnlPoolControls.Controls.Add(Me.btnDeleteContestant)
        Me.pnlPoolControls.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlPoolControls.Location = New System.Drawing.Point(3, 240)
        Me.pnlPoolControls.Name = "pnlPoolControls"
        Me.pnlPoolControls.Size = New System.Drawing.Size(588, 60)
        Me.pnlPoolControls.TabIndex = 0
        '
        'lblPlayerCount
        '
        Me.lblPlayerCount.AutoSize = True
        Me.lblPlayerCount.Location = New System.Drawing.Point(534, 24)
        Me.lblPlayerCount.Name = "lblPlayerCount"
        Me.lblPlayerCount.Size = New System.Drawing.Size(13, 13)
        Me.lblPlayerCount.TabIndex = 3
        Me.lblPlayerCount.Text = "0"
        '
        'lblPlayerCount_h
        '
        Me.lblPlayerCount_h.AutoSize = True
        Me.lblPlayerCount_h.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlayerCount_h.Location = New System.Drawing.Point(425, 24)
        Me.lblPlayerCount_h.Name = "lblPlayerCount_h"
        Me.lblPlayerCount_h.Size = New System.Drawing.Size(106, 13)
        Me.lblPlayerCount_h.TabIndex = 2
        Me.lblPlayerCount_h.Text = "Total Pool Count:"
        '
        'btnClearPool
        '
        Me.btnClearPool.Location = New System.Drawing.Point(253, 13)
        Me.btnClearPool.Name = "btnClearPool"
        Me.btnClearPool.Size = New System.Drawing.Size(141, 34)
        Me.btnClearPool.TabIndex = 1
        Me.btnClearPool.Text = "Reset/Clear Pool"
        Me.btnClearPool.UseVisualStyleBackColor = True
        '
        'btnDeleteContestant
        '
        Me.btnDeleteContestant.Enabled = False
        Me.btnDeleteContestant.Location = New System.Drawing.Point(83, 13)
        Me.btnDeleteContestant.Name = "btnDeleteContestant"
        Me.btnDeleteContestant.Size = New System.Drawing.Size(141, 34)
        Me.btnDeleteContestant.TabIndex = 0
        Me.btnDeleteContestant.Text = "Delete Contestant(s)"
        Me.btnDeleteContestant.UseVisualStyleBackColor = True
        '
        'MusicFileDialog
        '
        Me.MusicFileDialog.Filter = "Any Music Files|*.mp3;*.wav;*.mp2;*.aiff;*.au;"
        Me.MusicFileDialog.SupportMultiDottedExtensions = True
        Me.MusicFileDialog.Title = "Open - Select a media file to use:"
        '
        'ofdPlainText
        '
        Me.ofdPlainText.Filter = "Plain Text File|*.txt|All files|*.*"
        Me.ofdPlainText.Title = "Select a Plain Text File"
        '
        'frmMainScreen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1129, 598)
        Me.Controls.Add(Me.tbController)
        Me.Controls.Add(Me.mmMainMenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MainMenuStrip = Me.mmMainMenu
        Me.MaximizeBox = False
        Me.Name = "frmMainScreen"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Contestant Picker - Main Screen"
        Me.mmMainMenu.ResumeLayout(False)
        Me.mmMainMenu.PerformLayout()
        Me.tbController.ResumeLayout(False)
        Me.tbPool.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpAboutBox.ResumeLayout(False)
        Me.grpAboutBox.PerformLayout()
        Me.grpSoundConfig.ResumeLayout(False)
        Me.grpSoundConfig.PerformLayout()
        Me.grpLogoPreview.ResumeLayout(False)
        CType(Me.pbPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpLogoInfo.ResumeLayout(False)
        Me.grpLogoInfo.PerformLayout()
        Me.grpBatch.ResumeLayout(False)
        Me.grpBatch.PerformLayout()
        Me.grpSinglePlayer.ResumeLayout(False)
        Me.grpSinglePlayer.PerformLayout()
        Me.tbContestant.ResumeLayout(False)
        Me.grpAudioControls.ResumeLayout(False)
        Me.grpLastContestant.ResumeLayout(False)
        Me.grpSelectedNames.ResumeLayout(False)
        CType(Me.dgvSelected, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpControlPanel.ResumeLayout(False)
        Me.grpControlPanel.PerformLayout()
        Me.grpContestantPool.ResumeLayout(False)
        Me.pnlDataGridView.ResumeLayout(False)
        CType(Me.dgvContestantPool, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlPoolControls.ResumeLayout(False)
        Me.pnlPoolControls.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ofdLogoSearch As OpenFileDialog
    Friend WithEvents mmMainMenu As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents tmrClock As Timer
    Friend WithEvents tbController As TabControl
    Friend WithEvents grpLogoInfo As GroupBox
    Friend WithEvents btnBrowseLogo As Button
    Friend WithEvents txtLogoFileName As TextBox
    Friend WithEvents grpBatch As GroupBox
    Friend WithEvents rbContestant As RadioButton
    Friend WithEvents rbTicket As RadioButton
    Friend WithEvents lblHighest_h As Label
    Friend WithEvents lblLowest_h As Label
    Friend WithEvents txtHighest As TextBox
    Friend WithEvents txtLowest As TextBox
    Friend WithEvents btnAddBatch As Button
    Friend WithEvents grpSinglePlayer As GroupBox
    Friend WithEvents btnAddContestant As Button
    Friend WithEvents lblContestant_h As Label
    Friend WithEvents txtContestantName As TextBox
    Friend WithEvents tbContestant As TabPage
    Friend WithEvents grpContestantPool As GroupBox
    Friend WithEvents pnlDataGridView As Panel
    Friend WithEvents dgvContestantPool As DataGridView
    Friend WithEvents ContestantName As DataGridViewTextBoxColumn
    Friend WithEvents ContestantID As DataGridViewTextBoxColumn
    Friend WithEvents pnlPoolControls As Panel
    Friend WithEvents lblPlayerCount As Label
    Friend WithEvents lblPlayerCount_h As Label
    Friend WithEvents btnClearPool As Button
    Friend WithEvents btnDeleteContestant As Button
    Friend WithEvents grpControlPanel As GroupBox
    Friend WithEvents btnShowPresenter As Button
    Friend WithEvents btnClearWinners As Button
    Friend WithEvents btnStartRandomizer As Button
    Friend WithEvents cbClearSelected As CheckBox
    Friend WithEvents btnStopRandomizer As Button
    Friend WithEvents grpSelectedNames As GroupBox
    Friend WithEvents dgvSelected As DataGridView
    Friend WithEvents SelectedName As DataGridViewTextBoxColumn
    Friend WithEvents SelectedID As DataGridViewTextBoxColumn
    Friend WithEvents tbPool As TabPage
    Friend WithEvents grpLogoPreview As GroupBox
    Friend WithEvents pbPictureBox As PictureBox
    Friend WithEvents grpAudioControls As GroupBox
    Friend WithEvents btnPlayBackground As Button
    Friend WithEvents btnKillSounds As Button
    Friend WithEvents grpLastContestant As GroupBox
    Friend WithEvents lblSelectedPlayer As Label
    Friend WithEvents btnPlaySelection As Button
    Friend WithEvents btnPlayDrums As Button
    Friend WithEvents grpSoundConfig As GroupBox
    Friend WithEvents lblDrumRoll_h As Label
    Friend WithEvents btnPreviewDrumRoll As Button
    Friend WithEvents btnBrowseDrumRoll As Button
    Friend WithEvents txtDrumRoll As TextBox
    Friend WithEvents lblSelectionRoll_h As Label
    Friend WithEvents btnPreviewSelectionRoll As Button
    Friend WithEvents btnBrowseSelectionRoll As Button
    Friend WithEvents txtSelectionRoll As TextBox
    Friend WithEvents lblBackgroundMusic_h As Label
    Friend WithEvents btnPreviewBackgroundMusic As Button
    Friend WithEvents btnBrowseBackgroundMusic As Button
    Friend WithEvents txtBackgroundMusic As TextBox
    Friend WithEvents grpAboutBox As GroupBox
    Friend WithEvents MusicFileDialog As OpenFileDialog
    Friend WithEvents lblCallToAction As Label
    Friend WithEvents txtCallToAction As TextBox
    Friend WithEvents lblShowLogo_h As Label
    Friend WithEvents lblWelcomeText As Label
    Friend WithEvents txtWelcomeText As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblFileCount As Label
    Friend WithEvents btnAddFromFile As Button
    Friend WithEvents btnBrowseTextFile As Button
    Friend WithEvents txtTextFileName As TextBox
    Friend WithEvents ofdPlainText As OpenFileDialog
    Friend WithEvents btnResetWinner As Button
    Friend WithEvents cbFrameDisplay As CheckBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents lblPoolCount As Label
    Friend WithEvents lblPoolCount_h As Label
    Friend WithEvents lblTitle As Label
    Friend WithEvents Label1 As Label
End Class
